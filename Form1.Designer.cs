﻿namespace GILSTesterFFTestStationSample
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.openLot_Button = new System.Windows.Forms.Button();
            this.closeLot_Button = new System.Windows.Forms.Button();
            this.comboBoxLotId = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxDelay = new System.Windows.Forms.ComboBox();
            this.buttonMachineVersionStatusManualCheck = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxToken = new System.Windows.Forms.ComboBox();
            this.buttonGetLocalIPAddress = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxRecipeName = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(450, 37);
            this.label1.TabIndex = 0;
            this.label1.Text = "Version Control Hub Sample";
            // 
            // openLot_Button
            // 
            this.openLot_Button.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openLot_Button.Location = new System.Drawing.Point(101, 198);
            this.openLot_Button.Margin = new System.Windows.Forms.Padding(4);
            this.openLot_Button.Name = "openLot_Button";
            this.openLot_Button.Size = new System.Drawing.Size(397, 89);
            this.openLot_Button.TabIndex = 2;
            this.openLot_Button.Text = "Open Lot";
            this.openLot_Button.UseVisualStyleBackColor = true;
            this.openLot_Button.Click += new System.EventHandler(this.openLot_Button_Click);
            // 
            // comboBoxLotId
            // 
            this.comboBoxLotId.FormattingEnabled = true;
            this.comboBoxLotId.Location = new System.Drawing.Point(252, 105);
            this.comboBoxLotId.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxLotId.Name = "comboBoxLotId";
            this.comboBoxLotId.Size = new System.Drawing.Size(243, 33);
            this.comboBoxLotId.TabIndex = 5;
            this.comboBoxLotId.Text = "Lot123";
            this.comboBoxLotId.SelectedIndexChanged += new System.EventHandler(this.comboBoxLotId_SelectedIndexChanged);
            this.comboBoxLotId.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBoxLotId_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(95, 108);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Lot ID";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(95, 409);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(283, 180);
            this.label3.TabIndex = 7;
            this.label3.Text = "Delay Before AUTO Check Machine Version Status (milliseconds)\r\n** Set 0 or Empty " +
    "to disable auto check and enable Manual Check button below\r\n";
            // 
            // comboBoxDelay
            // 
            this.comboBoxDelay.FormattingEnabled = true;
            this.comboBoxDelay.Items.AddRange(new object[] {
            "0",
            "500",
            "750",
            "1000",
            "2000",
            "3000",
            ""});
            this.comboBoxDelay.Location = new System.Drawing.Point(386, 409);
            this.comboBoxDelay.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxDelay.Name = "comboBoxDelay";
            this.comboBoxDelay.Size = new System.Drawing.Size(111, 33);
            this.comboBoxDelay.TabIndex = 8;
            this.comboBoxDelay.Text = "0";
            this.comboBoxDelay.SelectedIndexChanged += new System.EventHandler(this.comboBoxDelay_SelectedIndexChanged);
            // 
            // buttonMachineVersionStatusManualCheck
            // 
            this.buttonMachineVersionStatusManualCheck.Enabled = false;
            this.buttonMachineVersionStatusManualCheck.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMachineVersionStatusManualCheck.Location = new System.Drawing.Point(102, 593);
            this.buttonMachineVersionStatusManualCheck.Margin = new System.Windows.Forms.Padding(4);
            this.buttonMachineVersionStatusManualCheck.Name = "buttonMachineVersionStatusManualCheck";
            this.buttonMachineVersionStatusManualCheck.Size = new System.Drawing.Size(396, 101);
            this.buttonMachineVersionStatusManualCheck.TabIndex = 9;
            this.buttonMachineVersionStatusManualCheck.Text = "Machine Version Status Manual Check";
            this.buttonMachineVersionStatusManualCheck.UseVisualStyleBackColor = true;
            this.buttonMachineVersionStatusManualCheck.Click += new System.EventHandler(this.buttonMachineVersionStatusManualCheck_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(95, 339);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 25);
            this.label4.TabIndex = 23;
            this.label4.Text = "Token";
            // 
            // comboBoxToken
            // 
            this.comboBoxToken.FormattingEnabled = true;
            this.comboBoxToken.Items.AddRange(new object[] {
            ""});
            this.comboBoxToken.Location = new System.Drawing.Point(252, 336);
            this.comboBoxToken.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxToken.Name = "comboBoxToken";
            this.comboBoxToken.Size = new System.Drawing.Size(243, 33);
            this.comboBoxToken.TabIndex = 22;
            this.comboBoxToken.SelectedIndexChanged += new System.EventHandler(this.comboBoxToken_SelectedIndexChanged);
            // 
            // buttonGetLocalIPAddress
            // 
            this.buttonGetLocalIPAddress.Location = new System.Drawing.Point(733, 593);
            this.buttonGetLocalIPAddress.Margin = new System.Windows.Forms.Padding(4);
            this.buttonGetLocalIPAddress.Name = "buttonGetLocalIPAddress";
            this.buttonGetLocalIPAddress.Size = new System.Drawing.Size(255, 101);
            this.buttonGetLocalIPAddress.TabIndex = 24;
            this.buttonGetLocalIPAddress.Text = "Get Local IP Address";
            this.buttonGetLocalIPAddress.UseVisualStyleBackColor = true;
            this.buttonGetLocalIPAddress.Click += new System.EventHandler(this.buttonTest_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 149);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 25);
            this.label5.TabIndex = 26;
            this.label5.Text = "Recipe Name";
            // 
            // comboBoxRecipeName
            // 
            this.comboBoxRecipeName.FormattingEnabled = true;
            this.comboBoxRecipeName.Location = new System.Drawing.Point(252, 146);
            this.comboBoxRecipeName.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxRecipeName.Name = "comboBoxRecipeName";
            this.comboBoxRecipeName.Size = new System.Drawing.Size(243, 33);
            this.comboBoxRecipeName.TabIndex = 25;
            this.comboBoxRecipeName.Text = "Recipe456";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(972, 804);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBoxRecipeName);
            this.Controls.Add(this.buttonGetLocalIPAddress);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBoxToken);
            this.Controls.Add(this.buttonMachineVersionStatusManualCheck);
            this.Controls.Add(this.comboBoxDelay);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.comboBoxLotId);
            this.Controls.Add(this.closeLot_Button);
            this.Controls.Add(this.openLot_Button);
            this.Controls.Add(this.label1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button openLot_Button;
        private System.Windows.Forms.Button closeLot_Button;
        private System.Windows.Forms.ComboBox comboBoxLotId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxDelay;
        private System.Windows.Forms.Button buttonMachineVersionStatusManualCheck;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxToken;
        private System.Windows.Forms.Button buttonGetLocalIPAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxRecipeName;
    }
}

