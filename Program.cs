﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;

using VersionControl.NET;

namespace GILSTesterFFTestStationSample
{

    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

#if DEBUG
            string exeFullPathName = "C:\\VersionControlHubCSharpSample-32bit\\VersionControlHubCSharpSample.exe";
#else
            string exeFullPathName = Application.ExecutablePath;
#endif

            string returnString = string.Empty;// "";
            string token = string.Empty;//null;

            string repoPath = System.IO.Path.GetDirectoryName(exeFullPathName);// + "\\";

            VersionControl.NET.Dll.CheckProgramVersion(
                exeFullPathName,
                repoPath,
                ref returnString,
                ref token,
                "",//"no lotId at program startup",
                "");//"no recipeName at program startup");

            Application.Run(new Form1());
        }
    }
}
