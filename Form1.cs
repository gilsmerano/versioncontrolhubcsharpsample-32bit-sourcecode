﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using VersionControl.NET;

namespace GILSTesterFFTestStationSample
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            timer_DelayBeforeAutomaticMachineVersionCheck = new System.Windows.Forms.Timer();
            timer_DelayBeforeAutomaticMachineVersionCheck.Tick += new EventHandler(timer_DelayBeforeAutomaticMachineVersionCheck_tick);

        }

        private void timer_DelayBeforeAutomaticMachineVersionCheck_tick(Object myObject, EventArgs myEventArgs)
        {
            timer_DelayBeforeAutomaticMachineVersionCheck.Stop();
            timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = false;

            Dll.MachineOpenLotVersionCheckInfo machineOpenLotFailedVersionCheckInfo =
                Dll.CheckMachineVersion(
                    comboBoxLotId.Text,
                    comboBoxToken.Text);

            if (machineOpenLotFailedVersionCheckInfo.machineProceedWithOpenLot)
                MessageBox.Show("Machine version check PASS");
            else
            {
                string failureDetails = "";

                for (int i = 0; i < machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos.Length; i++)
                {
                    failureDetails +=
                        machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].ipAddress
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].programType
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].failureInfo
                        + "\n\n";
                }

                MessageBox.Show(string.Format(
                    "Machine version check FAILED:\n\n{0}",
                    failureDetails));
            }
        }


        private void openLot_Button_Click(object sender, EventArgs e)
        {
#if DEBUG
            string exeFullPathName = "C:\\VersionControlHubCSharpSample-32bit\\VersionControlHubCSharpSample.exe";
#else
            string exeFullPathName = Application.ExecutablePath;
#endif

            string returnString = "";
            comboBoxToken.Text = "";
            string token = comboBoxToken.Text;
            string lotId = comboBoxLotId.Text;
            string recipeName = comboBoxRecipeName.Text;

            string repoPath = System.IO.Path.GetDirectoryName(exeFullPathName);// + "\\";

            VersionControl.NET.Dll.CheckProgramVersion(
                exeFullPathName,
                repoPath,
                ref returnString,
                ref token,
                lotId,
                recipeName);

            comboBoxToken.Text = token;

            int i = 0;
            string delay = comboBoxDelay.Text;
            bool isNumeric = int.TryParse(delay, out i);

            if (comboBoxDelay.Text != "0" && comboBoxDelay.Text != "" && isNumeric)
            {
                timer_DelayBeforeAutomaticMachineVersionCheck.Interval = Convert.ToInt32(comboBoxDelay.Text);
                timer_DelayBeforeAutomaticMachineVersionCheck.Enabled = true;
                timer_DelayBeforeAutomaticMachineVersionCheck.Start();
            }
        }

        private static System.Windows.Forms.Timer timer_DelayBeforeAutomaticMachineVersionCheck;

        private void comboBoxDelay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxDelay.Text == "0" || comboBoxDelay.Text == "")
            {
                buttonMachineVersionStatusManualCheck.Enabled = true;
            }
            else
            {
                buttonMachineVersionStatusManualCheck.Enabled = false;
            }
        }

        private void buttonMachineVersionStatusManualCheck_Click(object sender, EventArgs e)
        {
            Dll.MachineOpenLotVersionCheckInfo machineOpenLotFailedVersionCheckInfo =
                Dll.CheckMachineVersion(
                    comboBoxLotId.Text,
                    comboBoxToken.Text);

            if (machineOpenLotFailedVersionCheckInfo.machineProceedWithOpenLot)
            {
                MessageBox.Show("Machine version check PASS");
            }
            else if (machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos != null)
            {
                string failureDetails = "";

                for (int i = 0; i < machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos.Length; i++)
                {
                    failureDetails +=
                        machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].ipAddress
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].programType
                        + ": " + machineOpenLotFailedVersionCheckInfo.programFailedVersionCheckInfos[i].failureInfo
                        + "\n\n";
                }

                MessageBox.Show(string.Format(
                    "Machine version check FAILED:\n\n{0}",
                    failureDetails));
            }
            else
            {
                MessageBox.Show(string.Format(
                    "Machine version check FAILED"));
            }
        }

        private void comboBoxLotId_MouseClick(object sender, MouseEventArgs e)
        {
            int i = 0;
            string s = comboBoxLotId.Text;
            bool result = int.TryParse(s, out i);

            if (result)
            {
                comboBoxLotId.Text = Convert.ToString(Convert.ToInt32(i) + 1);
            }
        }

        private void comboBoxLotId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void buttonTest_Click(object sender, EventArgs e)
        {
            //string localIPAddress = Dll.GetLocalIPAddress();

            //MessageBox.Show(localIPAddress);
        }

        private void comboBoxToken_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}
